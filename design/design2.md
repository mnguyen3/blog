## Inspirations

### Blogs
* [Side panel layout](http://jquery-plugins.net/text-effects-sci-fi-style-effect-with-jquery)
* [Fade in animations](https://graphicfusiondesign.com/design/creating-fancy-css3-fade-in-animations-on-page-load/)
* [Single column blog template](https://mmistakes.github.io/hpstr-jekyll-theme/)
* [Widescreen images](http://mmistakes.github.io/skinny-bones-jekyll/)
* [Gray background](http://blog.peramid.es/)
* [Card blog posts](http://blog.hyfather.com/)
* [Dark theme](http://cocoaintheshell.com/)
* [Large fonts](http://hmfaysal.github.io/Notepad/)
	- A bit sluggish though (janky?)
* [Modus Create](http://moduscreate.com/blog/)
* [Composito theme](http://designdisease.com/preview/compositio)
	- Free theme under Creative Commons Attribution-Shared Alike 3.0
	- Take squares and animate them?
	- Copyleft license

### Theme creators
* [Design Disease](http://designdisease.com/)
	- Like the header stacked bars
	- Also like the colored circle bubbles

### Visualization blogs
* [Flowing data](http://flowingdata.com/)
* [Mike Bostock](http://bost.ocks.org/mike/algorithms/)
	- Not responsive [Homepage](http://bost.ocks.org/mike/)
* [Information is beautiful](http://www.informationisbeautiful.net/)
* [DataVisualization](http://datavisualization.ch/)
* [Eager eyes](https://eagereyes.org/)
* [Chartporn](http://chartporn.org/)
	- Like its layout
	- Using a wordpress theme. Somewhat slow?

### Cool charts
* [Strava heatmap](http://labs.strava.com/heatmap/#6/-120.90000/38.36000/blue/bike)

## Ideas

* Sub-blogs
	- Coding
	- Design
	- Visualizations
* Image focused blog posts?
* Homepage should use d3, angular, or something exciting / cool. Maybe a circle graph
* Allow for easy post navigation
	- Require user to click or scroll: which requires less effort?
* Allow for related posts?
* Dark and light theme?
* Improve syntax highlighting
	- Dark colors that expand entire width
* Multiple columns for blog posts?
	- Left side is posts
	- Right side is archives or related posts
	- Need to expand width
* Sci fi theme

## Post ideas

* Tutorial for creating a plugin architecture Java build with Maven modules?
* Tutorial with Gulp?

## Requirements

* Must be responsive in different browsers
* Must be fast
* Must be google searchable
* Must use a build tool to concatenate, minimize, all that (gulp?)

## Design Goals

* Emotions: fun, exciting, inspiring
	- Some type of interactive homepage
		* Starships and fighters randomly attacking each other?
		* Night sky with pulsing stars

## Resources

* [Turf](http://turfjs.org/examples.html)
