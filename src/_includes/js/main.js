var App = {};
App.Main = (function () {

    var initialize = function () {
        // set up navigation
        $('#boxNav').click(function () {
            window.location = '/';
        });
        $('#homeLink').click(function () {
            window.location = '/';
        });
        $('#projectsLink').click(function () {
            window.location = '/projects.html';
        });

        $('#showAnswer').click(function () {
            $('#hidden').slideToggle();
        });
        $('.lightbox').fluidbox();
        prettyPrint();
    }

    return {
        initialize: initialize
    };
});

var main = new App.Main();
main.initialize();

var showMorePosts = function() {
	var button = $('#showMorePostsButton');
	switch(button.text()) {
		case 'Show more posts':
			button.html('Hide extra posts');
			$('.listing-extra-posts').each(function(index) {
				$(this).removeClass('display-none');
			});
			break;
		default:
			button.html('Show more posts');
			$('.listing-extra-posts').each(function(index) {
				$(this).addClass('display-none');
			});
			break;
	}



}